import React, { FC } from 'react';
import { TbNotes } from 'react-icons/tb'
import { TiBeer} from 'react-icons/ti'

interface HeaderProps {
  title: string;
  subtitle?: string;
}

const Header: FC<HeaderProps> = ({ title, subtitle }) => {
  return (
    <header className="hero has-text-centered has-background-grey-darker is-bold mb-5">
      <div className="hero-body">
        <div className="container">
          <h1 className="title py-1 has-text-info-light "><TbNotes className=" mx-1 has-text-info "/> {title} <TiBeer className=" mx-1 has-text-info "/></h1>
          <h2 className="subtitle has-text-link-light ">{subtitle}</h2>
        </div>
      </div>
    </header>
  );
}

export default Header;