import React, { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Checkbox from '@mui/material/Checkbox';


import { Task } from '../store/types';
import { setTaskToDelete, setTaskToEdit } from '../store/actions';
import { RootState } from '../store/store';


interface TasksProps {
  tasks: Task[];
}

const Tasks: FC<TasksProps> = ({ tasks }) => {
  
  const dispatch = useDispatch();
  const list = useSelector((state: RootState) => state.list.selectedList!);

  
  
 
  
  

  const setTaskToEditHandler = (task: Task) => {
    dispatch(setTaskToEdit(task, list));
    
  }

  const setTaskToDeleteHandler = (task: Task) => {
    dispatch(setTaskToDelete(task, list));
  }

  
  const tasksTable = (
    <table className="table is-striped is-fullwidth">
      <tbody>
        {
          tasks.map((task: Task) => (
            
            <tr key={task.id} className={task.completed ? ' columns is-vcentered completed is-size-5' : 'columns is-vcentered is-size-5 py-1'}>
              <td className= 'column is-8 is-size-5' onClick={() => setTaskToEditHandler(task) }>{task.name}</td>
              <Checkbox  className="column h-12" size='small' />
              <td className=" column has-text-right ">
                <span className="is-size-5 has-text-danger" onClick={() => setTaskToDeleteHandler(task)}>
                  <i className="fas fa-times-circle"></i>
                </span>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );

  return(
    <section className="section">
      <h2 className="is-size-4 mb-6 has-text-centered">List of tasks in selected list</h2>
      {tasks.length === 0 ? <p className="py-4 has-text-centered">No Tasks</p> : tasksTable}
    </section>
  );
}

export default Tasks;